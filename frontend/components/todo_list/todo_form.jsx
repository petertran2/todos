import React from 'react'
import { connect } from 'react-redux'
import { receiveTodo } from './../../actions/todo_actions'

const mapDispatchToProps = dispatch => ({
  receiveTodo: todo => dispatch(receiveTodo(todo))
})

class Form extends React.Component {
	constructor(props) {
		super(props)
		this.initialState = {
			id: '',
			title: '',
			body: '',
			done: false
		}
		this.state = this.initialState
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}
	
	handleChange(change) {
		if (change.target.type === 'checkbox') {
			this.setState({[change.target.id]: change.target.checked})
		} else {
			this.setState({[change.target.id]: change.target.value})
		}
	}
	
	handleSubmit(whenSubmit) {
		whenSubmit.preventDefault()
		const { title } = this.state
		const { body } = this.state
		const { done } = this.state
		this.props.receiveTodo({ id: new Date().getTime(), title, body, done })
		this.setState(this.initialState)
	}
	
	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<label htmlFor='title'>Title</label>
				<input
					type='text'
					id='title'
					value={this.state.title}
					onChange={this.handleChange}
				/>
				<label htmlFor='body'>Body</label>
				<input
					type='text'
					id='body'
					value={this.state.body}
					onChange={this.handleChange}
				/>
				<label htmlFor='done'>Done</label>
				<input
					type='checkbox'
					id='done'
					checked={this.state.done}
					onChange={this.handleChange}
				/>
				<button type='submit'>Save</button>
			</form>
		)
	}
}

const TodoForm = connect(null, mapDispatchToProps)(Form)
export default TodoForm
