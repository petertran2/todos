import React from 'react'

export default function TodoListItem(props) {
	return (
		<li key={props.todo.id}>
			{props.todo.title}
			<button onClick={() => props.removeTodo(props.todo)}>Delete</button>
		</li>
	)
}
