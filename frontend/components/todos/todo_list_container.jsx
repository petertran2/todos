import { connect } from 'react-redux'
import { default as TodoList } from './todo_list'
import { receiveTodo, removeTodo } from './../../actions/todo_actions'

const mapStateToProps = state => ({
  todos: Object.keys(state.todos).map(id => state.todos[id])
});

const mapDispatchToProps = dispatch => ({
  receiveTodo: todo => dispatch(receiveTodo(todo)),
  removeTodo: todo => dispatch(removeTodo(todo))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
