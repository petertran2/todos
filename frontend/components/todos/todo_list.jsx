import React from 'react'
import TodoListItem from './../todo_list/todo_list_item'
import TodoForm from './../todo_list/todo_form'

export default ({ todos, removeTodo }) => (
	<div>
		<ul>
			{todos.map(todo => (
				<TodoListItem
					todo={todo}
					removeTodo={removeTodo}
				/>
			))}
		</ul>
		<TodoForm />
	</div>
)
