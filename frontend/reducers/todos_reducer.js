import { RECEIVE_TODOS,
	RECEIVE_TODO,
	REMOVE_TODO } from './../actions/todo_actions'

const initialState = {
  1: {
    id: 1,
    title: "wash car",
    body: "with soap",
    done: false
  },
  2: {
    id: 2,
    title: "wash dog",
    body: "with shampoo",
    done: true
  }
}

const todosReducer = (state = initialState, action) => {
	let nextState = {}
	switch (action.type) {
		case RECEIVE_TODOS:
			action.todos.forEach(todo => nextState[todo.id] = todo)
			return nextState
		case RECEIVE_TODO:
			return {...state, [action.todo.id]: action.todo}
		case REMOVE_TODO:
			nextState = {...state} //Do not set to 'state', it needs to be a completely different Object, alternatively can set to Object.assign({}, state)
			delete nextState[action.todo.id]
			return nextState
		default:
			return state
	}
}

export default todosReducer
